layout: post
title: Gitlab CI 运行成功
comments: true
date: 2017-10-10 16:52:00  +0800
updated:
tags:
- 随笔
- 技术
category:
- 技术

---

配置了好几天的 gitlab ci 终于运行成功了，真是不容易，各种配置注意事项真是太隐蔽了。

大部分配置看文档就能解决好，最大的一个问题却是文档没有提到的，那就是 gitlab-runer 默认开启了一些 Shared Runners。正是这些 runner 的默认开启，导致 pipelines 总是执行失败。只要关闭它们，就能正常执行自定义的 runner 了。

另外，最后还执行了：`gitlab-runner run` 才使 jobs 开始执行，后续应该不需要再这样了。

详细配置单独再说明吧。

至此，通过手机编辑并发布基于 hexo 生成的网站基本验证完成，下一步可以配置完整的工作流了。
