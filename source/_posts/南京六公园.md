layout: post
title: 南京六公园
comments: true
date: 2017-10-18 20:58:00 +0800
updated:
tags:
- 随笔
- 打油诗
category:
- 随笔

---

![南京南湖公园夜景](https://gitlab.com/MelerPaine/images/raw/master/Original/%E5%8D%97%E4%BA%AC%E5%8D%97%E6%B9%96%E5%85%AC%E5%9B%AD%E5%A4%9C%E6%99%AF.jpg)

白马公园无白马
白鹭公园找白鹭
莫愁公园你莫愁
百家湖边家家胡
南湖公园却难胡
玄武公园更玄乎

> 笔者注：南京环境好的公园特别多，仅挑了几个笔者熟悉的说说，大家有空多去逛逛，一定让你心情舒畅。

<!-- more -->

----
题图：南京南湖公园夜景@镜画者

**“你的喜爱就是我的动力，欢迎各位打赏”**
![微信赞赏码](https://user-images.githubusercontent.com/6366798/31317194-4971732c-ac6f-11e7-85b1-4ab24d057d8b.JPG)