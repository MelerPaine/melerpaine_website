layout: post
title: 如何在 table view 中添加 3D Touch Peek & Pop 功能
comments: true
date: 2017-11-23 09:00:00  +0800
updated:
tags:
- 技术
- iOS
category:
- 技术

---

![Redd Angelo](https://images.unsplash.com/photo-1428790067070-0ebf4418d9d8?auto=format&fit=crop&w=1500&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D)



Peek & Pop 在 iPhone 中是很实用的一个硬件相关特色功能，既可以提高操作效率，又有清晰的视觉表达。

Peek & Pop 是两个过程的组合，Peek 代表轻按屏幕激活预览窗口（会不会联想到在电脑中鼠标滑过链接时给出的提示，但这里视觉上更丰富了），Pop 代表继续重(zhòng)按屏幕打开刚才的预览窗口，如果只是轻按一下屏幕，预览窗口随着手指拿起就消失了。

<!-- more -->

实现这个功能最简单的方法就是通过 `Storyboard` 创建 `Segue`(手动触发模式的 `Segue` 除外)，并在 `Segue` 上勾选 `Peek & Pop`。但是如果想定制展现过程，就要通过编码的方式显示了。

通过编码实现这个功能的要点之一是提供一个用于预览的视图对象，而提供这个对象有 3 种不同的方法（一般教程中都未提到）：

1. 通过完整的程序编码创建并初始化一个视图控制器。
2. 通过 `XIB` 设计视图、自定义视图控制器类文件，并通过 `init(nibName:bundle:)` 方法初始化视图。
3. 通过 `Storyboard` 设计好视图，再通过 `Storyboard` 的 `instantiateViewController` 方法初始化这个视图。

网上的其他教程基本都是描述的方法 1（包含开头提到最简单的方法），方法 2、3 几乎没有提及。而方法 2、3 也是最容易出错的地方。

创建视图的过程就不多说了，用哪个方法都可以，重点是初始化它。假设我们创建的视图控制器类名称是: `PreviewingViewController`。

- 方法 1 直接用自定义的初始化方法就可以了（初始化方法甚至可以不写），最典型就是: `PreviewingViewController()`。
- 方法 2、3 如果还用 `PreviewingViewController()` ，接下来就等着报错和不停找问题了，笔者当时就在这里耗了很多时间。因为方法 2、3 都是通过 UI 文件创建的视图，它们的初始化方法只能使用特定的、也是标准的。具体说就是：
  - 使用 `XIB` 方式创建的视图，要使用 `init(nibName:bundle:)` 方法初始化。
  - 通过 `Storyboard` 创建的视图，要使用 `instantiateViewController` 方法初始化。

以上初始化方法掌握了，接下来就简单了，完成 `Peek & Pop` 一共三步。假设源视图是 `MainViewController`，要预览的是 `PreviewingViewController`：

- 让 `MainViewController` 遵守 `UIViewControllerPreviewingDelegate` 协议，并在其 `viewDidLoad()` 方法中注册 `Peek & Pop`:

```swift
if traitCollection.forceTouchCapability == .available {
    registerForPreviewing(with: self, sourceView: tableView) // sourceView 使用需要触发的 view 即可
}
```

- 添加代理方法提供预览的视图: `previewingContext(_:viewControllerForLocation:)`:

```swift
// Peek 操作
func previewingContext(_ previewingContext: UIViewControllerPreviewing,
                       viewControllerForLocation location: CGPoint) -> UIViewController?
{
    // 获取被按压的 Cell
    guard
    let indexPath = tableView.indexPathForRow(at: location),
    let cell = tableView.cellForRow(at: indexPath)
    else {
        return nil
    }
    // 按压时聚焦 Cell
    // 按压时要聚焦的区域都可以定制，提供你需要的就行。
    previewingContext.sourceRect = cell.frame

    // 根据上文讨论的，提供相应的初始化方法，这里以 storyboard 为例。
    let previewVC = self.storyboard?.instantiateViewController(
        withIdentifier: "xxx") as! PreviewingViewController
    // 把预览需要的信息传递过去
    previewVC.xxx = self.xxx
    return previewVC
}   
```

- 添加代理方法打开预览视图: `previewingContext(_:commit:)`：

```swift
// Pop 操作
func previewingContext(_ previewingContext: UIViewControllerPreviewing,
                           commit viewControllerToCommit: UIViewController)
{
    // 这里使用的条件判断，让你在某些情况下不触发 Pop 操作。
    if xxx {
        show(viewControllerToCommit, sender: self)
        // 根据打开视图的方式选择 show 或 present
        // present(viewControllerToCommit, animated: true)
    }
}
```


- - - -
题图：[Redd Angelo @unsplash](https://unsplash.com/photos/REyoFHP9pw8)

**“你的喜爱就是我的动力，欢迎各位打赏”**
![微信赞赏码](https://gitlab.com/MelerPaine/images/raw/master/Original/微信赞赏码.jpg)